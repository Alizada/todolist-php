
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>To do Lis</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<?php
echo "<button  onclick='document.location.href = `add.php`' >Add Task</button>";
include 'connect.php';
$cmd = "select * from toDoList";
$result = $conn->query($cmd);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $i = $row["id"];
        $task = $row["task"];
        $desc = $row["description"];
        $done = $row["done"];
        $text = "";
        $mark = "Mark as Completed";
        if($done == 1) {
            $text = "line-through";
            $mark = "Mark as Uncompleted";
        }
        echo "
        <div style=\"border: 1px solid red\">
            <h3 style='text-decoration: $text' >$task</h3>
            <p>$desc</p>
            <button onclick='document.location.href = `update.php?id=$i` '>Update task</button>
            <button onclick='document.location.href = `delete.php?id=$i` '>Delete Task</button>
            <button onclick='document.location.href = `done.php?id=$i` '>$mark</button>
        </div>";}

} else {
    echo "0 results";
}

?>

<script src ="js/script.js"></script>
</body>
</html>